FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ./src/ ./
RUN dotnet restore "Otus.Teaching.PromoCodeFactory.sln"
RUN dotnet build "Otus.Teaching.PromoCodeFactory.sln" -c Release -o /app/build
RUN dotnet publish "Otus.Teaching.PromoCodeFactory.WebHost/Otus.Teaching.PromoCodeFactory.WebHost.csproj" -c Release -o /app/publish


FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app

ENV ASPNETCORE_URLS=http://*:5000
EXPOSE 5000

COPY --from=build /app/publish ./
CMD dotnet Otus.Teaching.PromoCodeFactory.WebHost.dll
