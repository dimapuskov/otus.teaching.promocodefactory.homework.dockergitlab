﻿using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
            
            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();
            
            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();
            
            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();
        }

        public void InitializeDb(Action action)
        {
            action.Invoke();

            if (!_dataContext.Employees.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();
            }

            if (!_dataContext.Preferences.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();
            }

            if (!_dataContext.Customers.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();
            }
        }
    }
}