﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
        public void InitializeDb(Action migrateAction);
    }
}